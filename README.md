# soup2

Maintained fork of [`soup`](https://gitlab.com/pwoolcoc/soup).

Inspired by the python library [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/), this is a layer on top of [html5ever](https://github.com/servo/html5ever)
that adds a different API for querying & manipulating HTML

[Documentation (latest release)](https://docs.rs/soup)

[Documentation (main)](http://kod.kristoff.gitlab.io/soup2/)

== Installation

In order to use, add the following to your `Cargo.toml`:

```toml
[dependencies]
soup = "0.5"
```

== Usage

```rust
// src/main.rs


use std::error::Error;

use reqwest;
use soup2::prelude::*;

fn main() -> Result<(), Box<dyn Error>> {
    let response = reqwest::get("https://google.com")?;
    let soup = Soup::from_reader(response);
    let some_text = soup.tag("p")
			.attr("class", "hidden")
			.find()
			.and_then(|p| p.text());
    OK(())
}

```

