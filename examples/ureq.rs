use soup2::prelude::*;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let resp = ureq::get("https://docs.rs/soup/0.5.1/soup/")
        .call()?
        .into_reader();
    let soup = Soup::from_reader(resp)?;
    let result = soup
        .tag("section")
        .attr("id", "main")
        .find()
        .and_then(|section| {
            section
                .tag("span")
                .attr("class", "in-band")
                .find()
                .map(|span| span.text())
        });
    assert_eq!(result, Some("Crate soup".to_string()));
    Ok(())
}
